
#include "include/pose.hpp"
#include <sstream>


cv::Mat Pose::translation()
{
	return _translation;
}

void Pose::setTranslation(const cv::Mat &translation)
{
	_translation = translation;
}

cv::Mat Pose::rotation() const
{
	return _rotation;
}

void Pose::setRotation(const cv::Mat &rotation)
{
	_rotation = rotation;
}

Pose::Pose()
{
}





