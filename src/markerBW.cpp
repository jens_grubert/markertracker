
#include "include/markerBW.hpp"

using namespace std;

int MarkerBW::markerID[5][5] = {
      {1,   0,   0,   0,   0},
      {0,   1,   1,   1,   0},
      {1,   0,   1,   1,   1},
      {1,   0,   1,   1,   1},
      {1,   0,   1,   1,   1}
  };

MarkerBW::MarkerBW() : id(0)
{
}

bool operator<(const MarkerBW &M1,const MarkerBW&M2)
{
  return M1.id<M2.id;
}

cv::Mat MarkerBW::rotate(cv::Mat in)
{
  cv::Mat out;
  in.copyTo(out);
  for (int i=0;i<in.rows;i++)
  {
    for (int j=0;j<in.cols;j++)
    {
      out.at<uchar>(i,j)=in.at<uchar>(in.cols-j-1,i);
    }
  }
  return out;
}

int MarkerBW::hammDistMarkerBW(cv::Mat bits)
{
    /*
  int ids[4][5]=
  {
    {1,0,0,0,0},
    {1,0,1,1,1},
    {0,1,0,0,1},
    {0,1,1,1,0}
  };
  */
    



    
  
  int dist=0;
  
  for (int y=0;y<MARKER_HEIGHT;y++)
 //for (int y=0;y<markerW;y++)
  {
      int sum=0;
      //now, count
      for (int x=0;x<MARKER_WIDTH;x++)      
      {
        sum += bits.at<uchar>(y,x) == markerID[y][x] ? 0 : 1;
      }      
    dist += sum;
  }
    
cout << "dist: " << dist << endl;
    
  
  return dist;
}

int MarkerBW::mat2id(const cv::Mat &bits)
{
  int val=0;
  for (int y=0;y<5;y++)
  {
    val<<=1;
    if ( bits.at<uchar>(y,1)) val|=1;
    val<<=1;
    if ( bits.at<uchar>(y,3)) val|=1;
  }
  return val;
}

int MarkerBW::getMarkerBWId(cv::Mat &MarkerBWImage,int &nRotations)
{
  assert(MarkerBWImage.rows == MarkerBWImage.cols);
  assert(MarkerBWImage.type() == CV_8UC1);
  
  cv::Mat grey = MarkerBWImage;

  // Threshold image
  cv::threshold(grey, grey, 125, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);

#ifdef SHOW_DEBUG_IMAGES
  cv::showAndSave("Binary MarkerBW", grey);
#endif

  //MarkerBWs  are divided in 7x7 regions, of which the inner 5x5 belongs to MarkerBW info
  //the external border should be entirely black
  
  int cellSize = MarkerBWImage.rows / 7;
  
  for (int y=0;y<7;y++)
  {
    int inc=6;
    
    if (y==0 || y==6) inc=1; //for first and last row, check the whole border
    
    for (int x=0;x<7;x+=inc)
    {
      int cellX = x * cellSize;
      int cellY = y * cellSize;
      cv::Mat cell = grey(cv::Rect(cellX,cellY,cellSize,cellSize));
      
      int nZ = cv::countNonZero(cell);

      if (nZ > (cellSize*cellSize) / 2)
      {
        return -1;//can not be a MarkerBW because the border element is not black!
      }
    }
  }
  
  cv::Mat bitMatrix = cv::Mat::zeros(5,5,CV_8UC1);
  
  //get information(for each inner square, determine if it is  black or white)  
  for (int y=0;y<5;y++)
  {
    for (int x=0;x<5;x++)
    {
      int cellX = (x+1)*cellSize;
      int cellY = (y+1)*cellSize;
      cv::Mat cell = grey(cv::Rect(cellX,cellY,cellSize,cellSize));
      
      int nZ = cv::countNonZero(cell);
      if (nZ> (cellSize*cellSize) /2) 
        bitMatrix.at<uchar>(y,x) = 1;
    }
  }
    
     cout << "bitMatrix: " << bitMatrix  <<endl;;
    /*
    cout << "bitMatrix: [";
    for (int y=0;y<5;y++) {
        for (int x=0;x<5;x++) {
            cout << bitMatrix.at<uchar>(y,x) << " ";
        }
    }
    cout << "]" << endl;
    */
  
  //check all possible rotations
  cv::Mat rotations[4];
  int distances[4];
  
  rotations[0] = bitMatrix;  
  distances[0] = hammDistMarkerBW(rotations[0]);
  
  std::pair<int,int> minDist(distances[0],0);
  
  for (int i=1; i<4; i++)
  {
    //get the hamming distance to the nearest possible word
    rotations[i] = rotate(rotations[i-1]);
    distances[i] = hammDistMarkerBW(rotations[i]);
    
    if (distances[i] < minDist.first)
    {
      minDist.first  = distances[i];
      minDist.second = i;
    }
  }
  
  nRotations = minDist.second;
  if (minDist.first == 0)
  {
    int matId = mat2id(rotations[minDist.second]);
    std::cout << "marker id: " << matId << std::endl;
    return mat2id(rotations[minDist.second]);
     
  }
  
  return -1;
}

void MarkerBW::drawContour(cv::Mat& image, cv::Scalar color) const
{
    float thickness = 2;

    cv::line(image, points[0], points[1], color, thickness, CV_AA);
    cv::line(image, points[1], points[2], color, thickness, CV_AA);
    cv::line(image, points[2], points[3], color, thickness, CV_AA);
    cv::line(image, points[3], points[0], color, thickness, CV_AA);
}