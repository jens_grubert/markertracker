Partially based on 
https://github.com/MasteringOpenCV/code/tree/master/Chapter2_iPhoneAR/Example_MarkerBasedAR/Example_MarkerBasedAR


Runtime Instructions:

1. print marker.png
2. determine marker width (black border) in cm
3. run markerTrackerTest with parameters:
markerTrackerTest <cameraId> <camera.xml> <markerSizeInCm>
e.g.:
markerTrackerTest 0 calib-logitech-c910.xml 9.2
You can reuse the camera calibration file from the project https://bitbucket.org/jens_grubert/cameracalibration/