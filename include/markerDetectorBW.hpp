
#ifndef _MarkerDetectorBW_HPP_
#define _MarkerDetectorBW_HPP_

#include <vector>
#include <opencv2/opencv.hpp>

#include "include/pose.hpp"
#include "include/markerBW.hpp"

class MarkerBW;

class MarkerDetectorBW
{
public:
  typedef std::vector<cv::Point>    PointsVector;
  typedef std::vector<PointsVector> ContoursVector;


  /**
   * Initialize a new instance of marker detector object
   */
    
  MarkerDetectorBW(cv::Mat cameraMatrix, cv::Mat distCoeffs, float marker3DWidth, float marker3DHeight);
  
  /// Searches for markers and fills the list of transformation for found markers
  void processFrame(const cv::Mat& frame);
  
  const std::vector<Pose>& getPoses() const;
  
protected:

  /// Main marker detection routine
	bool findMarkers(const cv::Mat& frame, std::vector<MarkerBW>& detectedMarkers);

  /// Converts image to grayscale
  void prepareImage(const cv::Mat& bgraMat, cv::Mat& grayscale) const;

  /// Performs binary threshold
  void performThreshold(const cv::Mat& grayscale, cv::Mat& thresholdImg) const;

  /// Detects appropriate contours
  void findContours(cv::Mat& thresholdImg, ContoursVector& contours, int minContourPointsAllowed) const;

  /// Finds marker candidates among all contours
  void findCandidates(const ContoursVector& contours, std::vector<MarkerBW>& detectedMarkers);
  
  /// Tries to recognize markers by detecting marker code 
  void recognizeMarkers(const cv::Mat& grayscale, std::vector<MarkerBW>& detectedMarkers);

  /// Calculates marker poses in 3D
  /// calculates the pose of the camera in the reference frame of the marker (markerCOS)
  /// todo: make optional to also calculate the pose of th emarker in the cameraCIS
  void estimatePosition(std::vector<MarkerBW>& detectedMarkers);

private:
  float m_minContourLengthAllowed;
  
  cv::Size markerImageSize;
  cv::Mat camMatrix;
  cv::Mat distCoeff;
  std::vector<Pose> m_transformations;
  
  cv::Mat m_grayscaleImage;
  cv::Mat m_thresholdImg;  
  cv::Mat canonicalMarkerImage;

  ContoursVector           m_contours;
  std::vector<cv::Point3f> m_markerCorners3d;
  std::vector<cv::Point2f> m_markerCorners2d;

  
  /// Calculates perimeter of marker corner points    
  float perimeter(const std::vector<cv::Point2f> &a);
    
  /// Checks if points lie within a contour. Not used for now.
  bool isInto(cv::Mat &contour, std::vector<cv::Point2f> &b);
};

#endif