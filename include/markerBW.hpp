#ifndef MarkerBW_hpp
#define MarkerBW_hpp


#include <vector>
#include <iostream>
#include <opencv2/opencv.hpp>

#include "include/pose.hpp"

#define MARKER_WIDTH 5
#define MARKER_HEIGHT 5

class MarkerBW
{  
public:
  MarkerBW();
  friend std::ostream & operator<<(std::ostream &str,const MarkerBW &M);
friend bool operator<(const MarkerBW &M1,const MarkerBW&M2);
    
 static cv::Mat rotate(cv::Mat  in);
 /// @brief calculates the hamming distance between the a given code and 
 static int hammDistMarkerBW(cv::Mat bits);
 /// binary to numerical id
 static int mat2id(const cv::Mat &bits);
 /// @brief get the marker id for the current marker
 /// @return the numerical marker id and the number of 90° rotations to orient it in the canonical / standard orientation    
 static int getMarkerBWId(cv::Mat &in,int &nRotations);
  
public:
  
  /// Binary id
  static int markerID[5][5];
  /// Numerical id
  int id; 
  
  /// MarkerBW transformation with regards to the camera
  Pose pose;
  /// The corner points of the marker rectangle
  std::vector<cv::Point2f> points;

  // Helper function to draw the marker contour over the image
  void drawContour(cv::Mat& image, cv::Scalar color = CV_RGB(0,250,0)) const;
};

#endif