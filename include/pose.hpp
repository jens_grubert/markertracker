#ifndef _POSE_HPP_
#define _POSE_HPP_ 1

#include <opencv2/core/core.hpp>

class Pose {
public:
	Pose();
	///@brief retrieve 3D translation
    /// @return 3 vector
	cv::Mat translation();
	void setTranslation(const cv::Mat &translation);
	/// @brief get orientation as rodrigues rotation vector.
/// http://docs.opencv.org/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#void Rodrigues(InputArray src, OutputArray dst, OutputArray jacobian)
	cv::Mat rotation() const;
	/// \brief set orientation as rodrigues rotation vector.
    void setRotation(const cv::Mat &rotation); ///< set the rotation component of the pose
protected:
	cv::Mat _translation;
	cv::Mat _rotation;
};
#endif